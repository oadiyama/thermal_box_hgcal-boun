from threading import Lock, Thread
import base64
from io import BytesIO
from matplotlib.figure import Figure
#from matplotlib.pyplot import plt
import numpy as np
import time,datetime

from flask_socketio import SocketIO, send, emit

class SingletonMeta(type):
    """
    This is a thread-safe implementation of Singleton.
    """

    _instances = {}

    _lock: Lock = Lock()
    """
    We now have a lock object that will be used to synchronize threads during
    first access to the Singleton.
    """

    def __call__(cls, *args, **kwargs):
        """
        Possible changes to the value of the `__init__` argument do not affect
        the returned instance.
        """
        # Now, imagine that the program has just been launched. Since there's no
        # Singleton instance yet, multiple threads can simultaneously pass the
        # previous conditional and reach this point almost at the same time. The
        # first of them will acquire lock and will proceed further, while the
        # rest will wait here.
        print("wait")
        with cls._lock:
            print("first")
            # The first thread to acquire the lock, reaches this conditional,
            # goes inside and creates the Singleton instance. Once it leaves the
            # lock block, a thread that might have been waiting for the lock
            # release may then enter this section. But since the Singleton field
            # is already initialized, the thread won't create a new object.
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]

class BackendWorker(Thread,metaclass=SingletonMeta):
    sockio : SocketIO = None
    def __init__(self,sockio : SocketIO ) -> None:
        self.sockio = sockio 
        print("init")
        Thread.__init__(self)
    def run(self):
        counter = 0
        fig = Figure()
        while True:
            time.sleep(.5)
            # ---------------------------------------------------
            counter = counter + 1
            print(counter)
            now = datetime.datetime.now()
            self.sockio.emit('msg',now.strftime("%Y-%m-%d %H:%M:%S" ))
            # --------------------------------------------------
            fig.clear()
            ax =  fig.add_subplot()
            arr = np.random.normal(-2, 1, size=3000)
            sr  = np.arange(0+(counter%62)/10,4*np.pi+(counter%62)/10,0.1)
            # ax.plot(np.sin(sr))
            ax.hist(arr, bins=100)
            ax.set_xlabel('Values')
            ax.set_ylabel('Number of Hits')
            ax.set_title("flask_server - Normaş Distribution ")
            # Save it to a temporary buffer.
            buf = BytesIO()
            fig.savefig(buf,dpi=100, format="jpg")
            # Embed the result in the html output.
            data = base64.b64encode(buf.getbuffer()).decode("ascii")
            img_ascii = f"<img src='data:image/png;base64,{data}'/>"
            self.sockio.emit('img_send',img_ascii )
            buf = None
            ax = None
        """
        Finally, any singleton should define some business logic, which can be
        executed on its instance.
        """