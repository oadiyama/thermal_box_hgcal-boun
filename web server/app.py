from flask import Flask,render_template,url_for,redirect,request
from flask_socketio import SocketIO, send, emit
import re
from BackendWorker import *

#from waitress import serve

application = Flask(__name__)
application.config['SECRET_KEY'] = 'b7ea6f4886c8a41fdbca36338d749e2296bd72785c7cb000d7504305046e34f4'

socketio = SocketIO(application)

# ----------- START BACKGROUND THREAD -----------------
worker =  BackendWorker(socketio)
if worker.is_alive() == False:
    print(worker.name)
    worker.start()
#-------------------------------------------------------

active_clients = 0
ch=np.zeros(56)
@application.route("/", methods=['GET','POST'])
def index_html():
    k=1 
    for sw in request.form:
        num=int(re.search(r'\d+', sw).group())
        if num<48 and k==1:
            ch[:48]=0
            k=2
        elif num>47 and k==1:
            ch[48:]=0
            k=2
        ch[num-1]=1
    worker.set(ch)
    return render_template('index.html',cht=ch)

@socketio.on('connect')
def test_connect():
    print('Client connected')
    global active_clients 
    active_client = active_clients + 1
    socketio.emit('client_update', active_clients)

@socketio.on('disconnect')
def test_disconnect():
    print('Client disconnected')



if __name__ == '__main__':
    # if worker.is_alive() == False:
    #     print(worker.name)
    #     worker.start()
    print("Starting socketIO...")
    socketio.run(application,host='0.0.0.0',port=3000,debug=False)
    print("Continuing socketIO...")
    #serve(app, host='0.0.0.0', port=8080)
    