# -*- coding: utf-8 -*-
"""
Siemens PLC 1500 series communicator
By Kıvanç Nurdan
TUBITAK@CERN
"""

import snap7
import ctypes
import time
import datetime
import struct
import sys

from typing import Union, Tuple, Optional, List

class plc1500Communicator(snap7.client.Client):
    def __init__(self, lib_location: Optional[str] = None) -> None:
        super().__init__(lib_location)
        
    def readData(self):
        if self.get_connected() == True :
            hmi_struct = ">hhhffffhfff"
            hmi_struct_sz = struct.calcsize(hmi_struct)
            nrmodules = int.from_bytes(self.db_read(444,0,2),byteorder="big")  # single values   
            t =  datetime.datetime.now()
            #<fname = f"control_data_%s.txt"%(t.strftime("%Y%m%d_%H%M%S"))
            #fptr = open(fname,"w")
            try:    
                data = self.db_read(444,2,nrmodules*hmi_struct_sz)
            except:
                pass 
            # for structured data
            values = []
            for i in range(nrmodules):
                values.append(struct.unpack(hmi_struct,data[i*hmi_struct_sz:(i+1)*hmi_struct_sz]))  
            dewpts = []
            relhmdty = []
            senstemp = []
            for i in range(nrmodules):
                relhmdty.append(values[i][6])
                senstemp.append(values[i][8])
                dewpts.append(values[i][9]) 
            t = datetime.datetime.now()    
            wrdata = [t] + senstemp +relhmdty+dewpts
            return wrdata
            #self.writedata(fptr, wrdata)   
    def writedata(self,dptr):
        sz = len(dptr)
        for i in range(sz):
            if type(dptr[i]) == float:
                fptr.write(f"%6.3f"%dptr[i])
            else:
                fptr.write(str(dptr[i]))
            if i != sz-1:
                fptr.write(",")
        fptr.write("\n")
        fptr.flush()    

if __name__ == "__main__":
    plc = plc1500Communicator()
    plc.connect("128.141.60.244",0,1)  
    for i in range(100):
        valarr = plc.readData()
        print(valarr)
        time.sleep(5)  