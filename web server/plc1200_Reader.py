# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import snap7
import ctypes
import time
import datetime
import struct
import sys

class plc1200Communicator():
    def __init__(self):
        self.nrslaves = 1
        self.dbID = 4
        self.nrslaves_sz = 2
        self.slvid_sz = 2
        self.avail_sz = 2
        self.slavestatus_sz = 2
        self.timestamp_sz = 12
        self.slavedata_sz = 80*2 # values are 16 bit words
        self.slaveIDs = []
        self.plc = snap7.client.Client()
    def __del__(self):
        self.plc.destroy()
    def connectPLC(self,ip,dbID=4):
        self.IP = ip
        self.dbID = dbID
        self.plc.connect(self.IP,0,1)
        if self.plc.get_connected() == True:
            self.nrslaves =  int.from_bytes(self.plc.db_read(dbID, 0, 2),byteorder='big')
            slvid = self.plc.db_read(dbID,2,  self.slvid_sz*self.nrslaves) 
            self.slaveIDs = []
            for i in range(self.nrslaves):
                self.slaveIDs.append(int.from_bytes(slvid[0+i* self.slvid_sz:2+i* self.slvid_sz],byteorder='big'))
            return True
        else:
            return False
    def getNrSlaves(self):
        return self.nrslaves
    def getSlaveIDs(self):
        return self.slaveIDs
    def readDBBlock(self):
        blksize =self.nrslaves_sz+self.slvid_sz*self.nrslaves+self.nrslaves*(self.avail_sz+self.slavestatus_sz+self.timestamp_sz+self.slavedata_sz)
        #print(blksize)
        self.blkdata = self.plc.db_read(self.dbID, 0, blksize )
    def readAllSlaves(self):
        self.readDBBlock()
        avail = []
        offset = self.nrslaves_sz + self.slvid_sz * self.nrslaves
        dataoffset = self.avail_sz + self.slavestatus_sz + self.timestamp_sz + self.slavedata_sz 
        #print(offset,dataoffset)
        for i in range(self.nrslaves):
            avail.append(int.from_bytes(self.blkdata[offset+i*dataoffset:offset+2+i*dataoffset],byteorder="big"))
        return avail
    def processSlave(self,slave):
        data = []
        data.append(slave+1)
        offset = self.nrslaves_sz + self.slvid_sz * self.nrslaves
        dataoffset = self.avail_sz + self.slavestatus_sz  + self.timestamp_sz 
        framesize = dataoffset+self.slavedata_sz
        readoffset = offset + dataoffset+ slave*(framesize) + 6 # 6 bytes fo header in data
        #print(offset,dataoffset,readoffset)
        status_offset = offset + self.avail_sz + slave*(framesize)
        # date_offset = offset + self.avail_sz + self.slavestatus_sz + slave*(framesize)
        # status = int.from_bytes(self.blkdata[status_offset:status_offset+2],byteorder="big")
        # year =  int.from_bytes(self.blkdata[date_offset:date_offset+2],byteorder="big")
        # month = int.from_bytes(self.blkdata[date_offset+2:date_offset+2+1],byteorder="big")
        # day = int.from_bytes(self.blkdata[date_offset+3:date_offset+3+1],byteorder="big")
        # hours = int.from_bytes(self.blkdata[date_offset+5:date_offset+5+1],byteorder="big")
        # mins = int.from_bytes(self.blkdata[date_offset+6:date_offset+6+1],byteorder="big")
        # secs = int.from_bytes(self.blkdata[date_offset+7:date_offset+7+1],byteorder="big")
             
        status,year,month,day,hours,mins,secs = struct.unpack(">hhbbxbbb",self.blkdata[status_offset:status_offset+10])
        #print(status_offset,self.blkdata[status_offset:status_offset+11])
        t = datetime.datetime(year,month,day,hours,mins,secs)
        data.append(t)
      #  print(t.strftime("%b %d %Y %H:%M:%S"),end=',')
        #print("%d. Slave --------------"%(slave+1)) 
        #print(t.strftime("%b %d %Y %H:%M:%S"))
        for i in range (24):
            valid = self.blkdata[readoffset+2+i*6]
            temp = int.from_bytes(self.blkdata[readoffset+3+i*6:readoffset+6+i*6],byteorder="big")
            # print(self.blkdata[readoffset+i*6:readoffset+6+i*6])
            #print(i,end=': ')
            if (temp & 0x00800000): 
                temp ^= 0x00ffffff
                val = temp / -1024.
            else :
                val = temp / 1024.
            if valid == 0x01:
                #print(val,end='')
                #print("%3d %8.2f"%(i+1,val))
                data.append(val)
            else :
               # print('x',end='')
                #print("%3d %s 0x%2x"%(i+1,'x------x',valid))
                data.append('x')
            # if i != 23 :
            #    # print(',',end='')
            #     pass
            # else :
            #     print()
        return data
    # def writeSlave(self,fptr,dptr):
    #     sz = len(dptr)
    #     for i in range(sz):
    #         fptr.write(str(dptr[i]))
    #         if i != sz-1:
    #             fptr.write(",")
    #     fptr.write("\n")
    #     fptr.flush()
    def writeSlave(self,fptr,dptr):
        sz = len(dptr)
        for i in range(sz):
            if type(dptr[i]) == float:
                fptr.write(f"%6.3f"%dptr[i])
            else:
                fptr.write(str(dptr[i]))
            if i != sz-1:
                fptr.write(",")
        fptr.write("\n")
        fptr.flush()   
    
if __name__ == "__main__":
    plc1200 = plc1200Communicator()
    plc1200.connectPLC("128.141.60.243")
    
    # ====
 #   plc1500 = snap7.client.Client()    
 #   plc1500.connect("128.141.60.244", 0, 1)
 #   plc1500.get_connected()
 #   plc1500.db_read(444, 0, 254+36)
    t =  datetime.datetime.now()
    fname = f"monitor_data_%s.txt"%(t.strftime("%Y%m%d_%H%M%S"))
    f = open(fname,mode='w')
    #plc.checkDone()
    nrloop = 1
    while True:
        print("-----------------------------------------")
        print(f"sample -> %d"%(nrloop))
        print("-----------------------------------------")
        try:
            plc1200.readAllSlaves()
        except snap7.Snap7Exception:
            continue
        
        #      controller = plc1500.db_read(444,0,254+36)
        #    print(controller)
            #print("----slave 1-------")    
        s1 = plc1200.processSlave(0)
        plc1200.writeSlave(f,s1)
            #print("----slave 2-------")
     #       print("2,",end='')
        s2 = plc1200.processSlave(1)
        plc1200.writeSlave(f,s2)
        try:        
            time.sleep(10)
        except KeyboardInterrupt:
            print("Ëxiting...")
            f.close()
            sys.exit()
            # to be handled Snap7Exception:
        else:
            continue
    f.close()
        
    