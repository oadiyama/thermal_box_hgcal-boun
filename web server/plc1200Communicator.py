# -*- coding: utf-8 -*-
"""
Siemens PLC 1500 series communicator
By Kıvanç Nurdan
TUBITAK@CERN
"""

import snap7
import ctypes
import time
import datetime
import struct
import sys

from typing import Union, Tuple, Optional, List

class plc1200Communicator(snap7.client.Client):
    def __init__(self, slaves = 1, dbID = 4, lib_location: Optional[str] = None) -> None:
        super().__init__(lib_location)
        self.nrslaves = slaves
        self.dbID = dbID
        self.nrslaves_sz = 2
        self.slvid_sz = 2
        self.avail_sz = 2
        self.slavestatus_sz = 2
        self.timestamp_sz = 12
        self.slavedata_sz = 80*2 # values are 16 bit words
        self.slaveIDs = []

    def readConfig(self):
        if self.get_connected() == True:
            self.nrslaves =  int.from_bytes(self.db_read(self.dbID, 0, 2),byteorder='big')
            slvid = self.db_read(self.dbID,2,  self.slvid_sz*self.nrslaves) 
            self.slaveIDs = []
            for i in range(self.nrslaves):
                self.slaveIDs.append(int.from_bytes(slvid[0+i* self.slvid_sz:2+i* self.slvid_sz],byteorder='big'))
            return True
        else:
            return False

    def getNrSlaves(self):
        return self.nrslaves

    def getSlaveIDs(self):
        return self.slaveIDs

    def readDataBlock(self):
        blksize =self.nrslaves_sz+self.slvid_sz*self.nrslaves+self.nrslaves*(self.avail_sz+self.slavestatus_sz+self.timestamp_sz+self.slavedata_sz)
        self.blkdata = self.db_read(self.dbID, 0, blksize )

    def processSlave(self,slave):
        data = []
        data.append(slave+1)
        offset = self.nrslaves_sz + self.slvid_sz * self.nrslaves
        dataoffset = self.avail_sz + self.slavestatus_sz  + self.timestamp_sz 
        framesize = dataoffset+self.slavedata_sz
        readoffset = offset + dataoffset+ slave*(framesize) + 6 # 6 bytes fo header in data
        status_offset = offset + self.avail_sz + slave*(framesize)
        status,year,month,day,hours,mins,secs = struct.unpack(">hhbbxbbb",self.blkdata[status_offset:status_offset+10])
        t = datetime.datetime(year,month,day,hours,mins,secs)
        data.append(t)

        for i in range (24):
            valid = self.blkdata[readoffset+2+i*6]
            temp = int.from_bytes(self.blkdata[readoffset+3+i*6:readoffset+6+i*6],byteorder="big")
            if (temp & 0x00800000): 
                temp ^= 0x00ffffff
                val = temp / -1024.
            else :
                val = temp / 1024.
            if valid == 0x01:
                data.append(val)
            else :
                data.append(-273.15)
        return data

    def writeSlave(self,fptr,dptr):
        sz = len(dptr)
        for i in range(sz):
            if type(dptr[i]) == float:
                fptr.write(f"%6.3f"%dptr[i])
            else:
                fptr.write(str(dptr[i]))
            if i != sz-1:
                fptr.write(",")
        fptr.write("\n")
        fptr.flush()   

if __name__ == "__main__":
    plc = plc1200Communicator()
    plc.connect("128.141.60.243",0,1)
    if plc.get_connected():
        plc.readConfig()  # do this once 
    else:
        exit()
    plc.readDataBlock()
    for i in range(plc.getNrSlaves()):
        slvdata = plc.processSlave(i)
        print(slvdata)