# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 14:05:38 2021

@author: knurdan
"""
# -------------------------------------------------------------------------------------------------------
import matplotlib
import numpy
import sys,getopt
from io import StringIO


plt = matplotlib.pyplot


def monitor_plot(figno,fname,chlist,N=0,S=1):
    # figno : defines the specific figure
    # fname : name of the data file
    # chlist : list of the channels to be plotted
    # N    : startting poisition in data file from the beginning of the record
    #        read all the samples if not given
    # S    : nr samples to be read 
    if N==0:
        data = numpy.genfromtxt(fname,delimiter=',')
    else:
        lines = readNlines(fname, N,S*2)
        data = numpy.genfromtxt(StringIO(lines),delimiter=',')

        
    # t1_1 = data[::2,2]
    # t1_7 = data[::2,8]
    # t1_13 = data[::2,14]
    # t1_24 = data[::2,25]  # every second row 
    # t2_7 = data[1::2,8]  # every second row 
    # t2_8 = data[1::2,9]  # every second row 
    # t2_13 = data[1::2,14]  # every second row 
    # t2_24 = data[1::2,25]
    # for creating the range in minutes
    datarng = int(len(data)/2)
    minindex = numpy.empty(datarng)
    numpy.put(minindex,range(datarng),range(datarng))
    minindex = minindex * 10 / 60   

    plt.figure(figno)
    plt.grid()

    # a = numpy.empty(len(t1_1))
    # numpy.put(a,range(len(t1_1)),range(len(t1_1)))
    # a = a * 10 / 60
    for i in chlist:
        if i <25:
            plt.plot(minindex,data[::2,i+1],label="t1_"+str(i))
        elif i < 49:
            y = i-24
            plt.plot(minindex,data[1::2,y+1],label="t2_"+str(y))
        else:
            # ignare list item
            pass

    # plotting
    #
    # plt.title("Thermal Box Monitor")
    # pltlines = plt.plot(a,t1_1,a,t1_7,a,t1_13,a,t1_24,a,t2_7,a,t2_8,a,t2_13,a,t2_24)
    # plt.legend(pltlines,["t1_1","t1_7","t1_13","t1_24","t2_7","t2_8(not touching)","t2_13","t2_24"])
    
    font1 = {'family':'serif','color':'blue','size':20}
    font2 = {'family':'serif','color':'darkred','size':15}
    
    plt.legend(ncol = 2)
    plt.title("Thermal Box Monitor", fontdict = font1,loc = 'left')
    plt.xlabel("Time (min)", fontdict = font2)
    plt.ylabel("Temperature (C)", fontdict = font2)
    plt.show()





# ------------------------------------------------------------------------------------------------------
# plt.subplot(1, 2, 1) # creating multiple plots

# # some fun
# tmod = t1_23 + abs(min(t1_23))
# tmod_color = tmod/max(tmod)*100
# plt.scatter(a,t1_23,c = tmod_color,cmap='viridis',s = 1)

# -------------------------------------------------------------------------------------------------------
def control_plot(figno,fname,chlist,N=0,S=1):
    # figno : defines the specific figure
    # fname : name of the data file
    # chlist : list of the channels to be plotted
    # N    : startting poisition in data file from the beginning of the record
    #        read all the samples if not given
    # S    : nr samples to be read     if N==0:
    if N==0:   
        ctrldata = numpy.genfromtxt(fname,delimiter=',')
    else:
        lines = readNlines(fname, N,S)
        ctrldata = numpy.genfromtxt(StringIO(lines),delimiter=',')
   # for the range in minutes
    datarng = len(ctrldata)
    minindex = numpy.empty(datarng)
    numpy.put(minindex,range(datarng),range(datarng))
    minindex = minindex * 10 / 60

    plt.figure(figno)
    xlabels = 3*["Time (min)"]
    ylabels = ["Temperature (C)","Rel Humidity (%)" ,"Dew Point (C)" ]
    for i in [0,1,2]: 
        plt.subplot(1,3,i+1)
        plt.grid()
        plt.ylabel(ylabels[i])
        plt.xlabel(xlabels[i])
        for j in chlist: 
            plt.plot(minindex,ctrldata[:,8*i+j],label=f"sensor%d"%j)
        plt.legend()
    

# --------------------------------------------------------------------------------------------------------

def readNlines(fname, N,S):
    # opening file using with() method
    # so that file get closed
    # after completing work
    lines = ""
    with open(fname) as file:
        # loop to read iterate 
        # last n lines and print it
        alllines=file.readlines()
        for line in alllines[N:N+S]:
            #print(line, end ='')
            lines=lines+line
    return lines
# data = numpy.genfromtxt(StringIO(lines),delimiter=',')

# # for cont'nous data stream 

# plt.ion()
# fig = plt.figure(figsize=(16,8))
# axes = fig.add_subplot(111)
# data_plot=plt.plot(0,0)
# line, = axes.plot([],[])
# for i in range(100):
#     x = range(i)
#     y = range(i)
#     line.set_ydata(y)
#     line.set_xdata(x)
#     if len(y)>0:
#         axes.set_ylim(min(y),max(y)+1) # +1 to avoid singular transformation warning
#         axes.set_xlim(min(x),max(x)+1)
#     plt.title(str(i))
#     plt.draw()
#     plt.pause(0.1)

# plt.show(block=True)

def main(args):
    plt = matplotlib.pyplot

    # outputfile = ''
    # try:
    #     opts, args = getopt.getopt(argv,"hm:c:"])
    # except getopt.GetoptError:
    #     print 'plot.py -i <inputfile> -ch <outputfile>'
    #     sys.exit(2)
    # for opt, arg in opts:
    #     if opt == '-h':
    #         print 'help me'
    #         sys.exit()
    #      elif opt in ("-m"):
    #         print (f"mon file %s"%inputfile)        
    #         inputfile = arg
    #         sys.exit()
    #     else:
    #       pass

if __name__ == "__main__":
    main(sys.argv[1:])