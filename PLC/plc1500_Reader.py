# -*- coding: utf-8 -*-
"""
Created on Wed Jul  7 11:08:26 2021

@author: knurdan
"""
import snap7
import struct
import ctypes
import datetime
import time

import sys

def writeSlave(fptr,dptr):
    sz = len(dptr)
    for i in range(sz):
        if type(dptr[i]) == float:
            fptr.write(f"%6.3f"%dptr[i])
        else:
            fptr.write(str(dptr[i]))
        if i != sz-1:
            fptr.write(",")
    fptr.write("\n")
    fptr.flush()

def main():
    plc1500 = snap7.client.Client()
    plc1500.connect("128.141.60.244",0,1)
    hmi_struct = ">hhhffffhfff"
    hmi_struct_sz = struct.calcsize(hmi_struct)
    if plc1500.get_connected() == True :
        plc1500.db_read(444,0,2)
        nrmodules = int.from_bytes(plc1500.db_read(444,0,2),byteorder="big")  # single values   
        t =  datetime.datetime.now()
        fname = f"control_data_%s.txt"%(t.strftime("%Y%m%d_%H%M%S"))
        fptr = open(fname,"w")
        loops = 0
        while True:
            try:    
                data = plc1500.db_read(444,2,nrmodules*hmi_struct_sz)
            except:
                continue
            # for structured data
            values = []
            for i in range(nrmodules):
                values.append(struct.unpack(hmi_struct,data[i*hmi_struct_sz:(i+1)*hmi_struct_sz]))  
            dewpts = []
            relhmdty = []
            senstemp = []
            for i in range(nrmodules):
                relhmdty.append(values[i][6])
                senstemp.append(values[i][8])
                dewpts.append(values[i][9]) 
            t = datetime.datetime.now()    
            wrdata = [t] + senstemp +relhmdty+dewpts
            writeSlave(fptr, wrdata)
            print(f'%6d. iter.    dewpt[3] = %6.3f \n                 dewpt[5] = %6.3f'%(loops,dewpts[2],dewpts[4]))           
            try:        
                time.sleep(10)
            except KeyboardInterrupt:
                print("Ëxiting...")
                fptr.close()
                plc1500.disconnect()
                sys.exit()
                # to be handled Snap7Exception:
            else:
                continue
        fptr.close()
        plc1500.disconnect()
if __name__ == "__main__":
    main()