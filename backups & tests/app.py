# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 14:49:27 2021

@author: oadiyama
"""

from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/shortenurl', methods=[ 'POST'])
def shortenurl():
    kl=[0, 0, 0, 0]
    for sw in request.form:
        if sw=='s1':
            kl[0]=1
        if sw=='s2':
            kl[1]=1
        if sw=='s3':
            kl[2]=1
        if sw=='s4':
            kl[3]=1
    return render_template('shortenurl.html',kl=kl)
  #  worker.setChannel(sw)
  #  return render_template('shortenurl.html')

if __name__ == "__main__":
    app.run(debug=True)