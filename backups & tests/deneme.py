# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 17:57:23 2021

@author: oadiyama
"""

from flask import Flask,render_template

app = Flask(__name__)

@app.route("/")
@app.route("/home")
def home():
    return render_template('index.html')

@app.route("/about")
def about():
    return "<h1>about page!</h1>"

if __name__ == "__main__":
    app.run(debug=True)