from threading import Lock, Thread
import base64
from io import BytesIO
from matplotlib.figure import Figure
#from matplotlib.pyplot import plt
import numpy as np
import time,datetime
# import snap7
# from plc1200Communicator import *
# from plc1500Communicator import *

from flask_socketio import SocketIO, send, emit
import sqlite3

class SingletonMeta(type):
    """
    This is a thread-safe implementation of Singleton.
    """

    _instances = {}

    _lock: Lock = Lock()
    """
    We now have a lock object that will be used to synchronize threads during
    first access to the Singleton.
    """

    def __call__(cls, *args, **kwargs):
        """
        Possible changes to the value of the `__init__` argument do not affect
        the returned instance.
        """
        # Now, imagine that the program has just been launched. Since there's no
        # Singleton instance yet, multiple threads can simultaneously pass the
        # previous conditional and reach this point almost at the same time. The
        # first of them will acquire lock and will proceed further, while the
        # rest will wait here.
        print("wait")
        with cls._lock:
            print("first")
            # The first thread to acquire the lock, reaches this conditional,
            # goes inside and creates the Singleton instance. Once it leaves the
            # lock block, a thread that might have been waiting for the lock
            # release may then enter this section. But since the Singleton field
            # is already initialized, the thread won't create a new object.
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]

class BackendWorker(Thread,metaclass=SingletonMeta):
    sockio : SocketIO = None
    def __init__(self,sockio : SocketIO ) -> None:
        self.sockio = sockio 
        print("init")
        # self.plc1200 = plc1200Communicator()
        # self.plc1200.connect("128.141.60.243",0,1)        
        # self.plc1500 = plc1500Communicator()
        # self.plc1500.connect("128.141.60.244",0,1)
        self.db_con = sqlite3.connect("static/combinedHistory.db",check_same_thread=False)
        self.db_con.row_factory=lambda cursor, row: row[0]
        self.historysize = 1000
        self.updateinterval = 10
        self.ch=np.zeros(24)
        Thread.__init__(self)
    def set(self,arr):
        self.ch=arr
    def run(self):
        counter = 0
        fig = Figure(figsize=(20,10))
        history_buf = []
        dewhistory_buf = []
        cursor = self.db_con.cursor()
        # if self.plc1200.get_connected():
        #     self.plc1200.readConfig()  # do this once 
        while True:
            # ---------------Update time Value onn web client -------------------------
            counter = counter + 1
            now = datetime.datetime.now()
            self.sockio.emit('time_msg',now.strftime("%Y-%m-%d %H:%M:%S")+f" - {counter}")
            # -------------- READING PLC 1200 ------------------------------------------
            # try:
            #     self.plc1200.readDataBlock()
            # except snap7.Snap7Exception: 
            #     continue
            # s1 = self.plc1200.processSlave(0)
            # s2 = self.plc1200.processSlave(1)
            # s1[1]=s1[1].timestamp()
            # s2[1]=s2[1].timestamp()
            card_num=1
            select = f"SELECT * FROM temperature_records WHERE pos="+f"{card_num} ORDER BY date DESC LIMIT 1"
            cursor.execute(select)
            s1 = cursor.fetchall()
            s1=np.array(s1)
            s1=s1[0]
            # print(type(s1))
            # print(s1)
            card_num=2
            select = f"SELECT * FROM temperature_records WHERE pos="+f"{card_num} ORDER BY date DESC LIMIT 1"
            cursor.execute(select)
            s2 = cursor.fetchall()
            s2=np.array(s2)
            s2=s2[0]
            select = f"SELECT * FROM humidity_records ORDER BY date DESC LIMIT 1"
            cursor.execute(select)
            h1 = cursor.fetchall()
            h1=np.array(h1)
            h1=h1[0]
            s1=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            s2=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            h1=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            # sqlcmd1 = f"INSERT INTO temperature_records  VALUES " + f"{s1}".replace("[","(").replace("]",")") 
            # sqlcmd2 = f"INSERT INTO temperature_records  VALUES " + f"{s2}".replace("[","(").replace("]",")") 
            #print(sqlcmd1) 
            # cursor.execute(sqlcmd1)           
            # cursor.execute(sqlcmd2)
            #cursor.execute(sqlcmd1)
            # ------------ READING PLC1500 ---------------------------------------------
            # h1 = self.plc1500.readData()
            # h1[0]=h1[0].timestamp()
            # sqlcmdh1 = f"INSERT INTO humidity_records  VALUES " + f"{h1}".replace("[","(").replace("]",")") 
            # cursor.execute(sqlcmdh1) 
            # ------ commit all transactions to database -------------------------------
            # self.db_con.commit()
            # ------ create history data for realtime monitoring -----------------------
            # history_buf.append(s1[2])
            # dewhistory_buf.append(h1[19])
            # if len(history_buf) > self.historysize:
            #     history_buf = history_buf[1:]
            #     dewhistory_buf = dewhistory_buf[1:]
            # --------Temperature plots  -----------------------------------------------
            fig.clear()
            ax1 =  fig.add_subplot(2,2,1)
            xaxis = np.arange(1,25)
            ax1.step(xaxis,s1[2:], where='mid')
            ax1.set_xlabel('Channels')
            ax1.set_ylabel('Temperature (C)')
            ax1.set_title(" Card (1) Temperature Sensors")
            ax1.grid()
            
            ax2 =  fig.add_subplot(2,2,3)
            ax2.step(xaxis,s2[2:], where='mid')
            ax2.set_xlabel('Channels')
            ax2.set_ylabel('Temperature (C)')
            ax2.set_title(" Card (2) Temperature Sensors")
            ax2.grid()

            bx = fig.add_subplot(2,2,(2,4))
            xvals = np.arange(self.historysize * self.updateinterval,0,-self.updateinterval)-self.updateinterval
            """if len(history_buf) < 1000:
                histfill = np.empty(1000-len(history_buf))
                histfill.fill(np.NaN)
                histfill[-1]=0 """
                
            card_num=1
            time_limit = time.time()-530000
            time_limit2 = time.time()-520000
            
            history_buf=[]
            select = f"SELECT c1 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c2 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c3 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c4 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c5 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c6 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c7 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c8 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c9 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c10 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c11 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c12 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c13 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c14 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c15 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c16 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c17 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c18 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c19 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c20 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c21 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c22 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c23 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            select = f"SELECT c24 FROM temperature_records WHERE date>"+f"{time_limit} AND date<"+f"{time_limit2} AND pos="+f"{card_num}"
            history_buf.append(cursor.execute(select).fetchall())
            # print(history_buf)
            # print(type(history_buf))
            # print(np.shape(history_buf))
            
            j=0
            while j<24:
                histbufsize = len(history_buf[j])
                histfill = np.empty(self.historysize-histbufsize)
                histfill.fill(np.NaN)
                if histbufsize < self.historysize:
                    histfill[0]=0
            # xvalsize = len(xvals)
            # histsize = len(history_buf)
            # allvals =  list(histfill) + history_buf
            # totalsize = len(allvals)
            # print(f"{histsize} -- {xvalsize}  {totalsize}")
                allvals = list(histfill) + history_buf[j]
            # allvals2 = list(histfill) + history_buf2
            # allvals3 = list(histfill) + history_buf3
            # allvals4 = list(histfill) + history_buf4
            # allvals5 = list(histfill) + history_buf5
            # allvals6 = list(histfill) + history_buf6
            # allvals7 = list(histfill) + history_buf7
            # allvals8 = list(histfill) + history_buf8
            # allvals9 = list(histfill) + history_buf9
            # allvals10 = list(histfill) + history_buf10
            # allvals11 = list(histfill) + history_buf11
            # allvals12 = list(histfill) + history_buf12
            # allvals13 = list(histfill) + history_buf13
            # allvals14 = list(histfill) + history_buf14
            # allvals15 = list(histfill) + history_buf15
            # allvals16 = list(histfill) + history_buf16
            # allvals17 = list(histfill) + history_buf17
            # allvals18 = list(histfill) + history_buf18
            # allvals19 = list(histfill) + history_buf19
            # allvals20 = list(histfill) + history_buf20
            # allvals21 = list(histfill) + history_buf21
            # allvals22 = list(histfill) + history_buf22
            # allvals23 = list(histfill) + history_buf23
            # allvals24 = list(histfill) + history_buf24
            # print(allvals)
            # print(allvals2)
            #print(xvals)
            ##allvals.reverse()
                if self.ch[j]==1:
                    bx.plot(xvals,allvals,'-', color="red")
                j=j+1
            # if self.ch[1]==1:
            #     bx.plot(xvals,allvals2,'-', color="blue")
            # if self.ch[2]==1:
            #     bx.plot(xvals,allvals3,'-', color="yellow")
            # if self.ch[3]==1:
            #     bx.plot(xvals,allvals4,'-', color="green")
            # if self.ch[4]==1:
            #     bx.plot(xvals,allvals5,'-', color="black")
            # if self.ch[5]==1:
            #     bx.plot(xvals,allvals6,'-', color="brown")
            # if self.ch[6]==1:
            #     bx.plot(xvals,allvals7,'-', color="pink")
            # if self.ch[7]==1:
            #     bx.plot(xvals,allvals8,'-', color="orange")
            # if self.ch[8]==1:
            #     bx.plot(xvals,allvals9,'-', color="purple")
            # if self.ch[9]==1:
            #     bx.plot(xvals,allvals10,'-', color="blue")
            # if self.ch[10]==1:
            #     bx.plot(xvals,allvals11,'-', color="blue")
            # if self.ch[11]==1:
            #     bx.plot(xvals,allvals12,'-', color="blue")
            # if self.ch[12]==1:
            #     bx.plot(xvals,allvals13,'-', color="blue")
            # if self.ch[13]==1:
            #     bx.plot(xvals,allvals14,'-', color="blue")
            # if self.ch[14]==1:
            #     bx.plot(xvals,allvals15,'-', color="blue")
            # if self.ch[15]==1:
            #     bx.plot(xvals,allvals16,'-', color="blue")
            # if self.ch[16]==1:
            #     bx.plot(xvals,allvals17,'-', color="blue")
            # if self.ch[17]==1:
            #     bx.plot(xvals,allvals18,'-', color="blue")
            # if self.ch[18]==1:
            #     bx.plot(xvals,allvals19,'-', color="blue")
            # if self.ch[19]==1:
            #     bx.plot(xvals,allvals20,'-', color="blue")
            # if self.ch[20]==1:
            #     bx.plot(xvals,allvals21,'-', color="blue")
            # if self.ch[21]==1:
            #     bx.plot(xvals,allvals22,'-', color="blue")
            # if self.ch[22]==1:
            #     bx.plot(xvals,allvals23,'-', color="blue")
            # if self.ch[23]==1:
            #     bx.plot(xvals,allvals24,'-', color="blue")
                
            bx.invert_xaxis()
            bx.set_xlabel('Time "Current time is 0" (s)')
            bx.set_ylabel('Temperature (C)')
            bx.set_title(" Sensor (1)")
            bx.grid()
            # Save it to a temporary buffer.
            buf = BytesIO()
            fig.savefig(buf,dpi=100, format="jpg")
            # Embed the result in the html output.
            data = base64.b64encode(buf.getbuffer()).decode("ascii")
            img_ascii = f"<img src='data:image/png;base64,{data}'/>"
            self.sockio.emit('tmp_send',img_ascii )
            # ------------- humidity plots ---------------------------
            fig.clear()
            xaxis = np.arange(1,9)
            ax1 =  fig.add_subplot(2,2,1)
            ax1.step(xaxis,h1[1:9], where='mid')
            ax1.set_xlabel('Channels')
            ax1.set_ylabel('Temperature (C)')
            ax1.set_title("  Dev point Measurement Temperature Sensors")
            ax1.grid()
            
            ax2 =  fig.add_subplot(2,2,3)
            ax2.step(xaxis,h1[9:17], where='mid')
            ax2.set_xlabel('Channels')
            ax2.set_ylabel('Rel Humidity (%)')
            ax2.set_title(" Dev point Measurement Humidity Sensors")
            ax2.grid()

            bx = fig.add_subplot(2,2,(2,4))
            xvals = np.arange(self.historysize * self.updateinterval,0,-self.updateinterval)-self.updateinterval
            """if len(history_buf) < 1000:
                histfill = np.empty(1000-len(history_buf))
                histfill.fill(np.NaN)
                histfill[-1]=0 """
            histbufsize = len(dewhistory_buf)
            histfill = np.empty(self.historysize-histbufsize)
            histfill.fill(np.NaN)
            if histbufsize < self.historysize:
                histfill[0]=0
            # xvalsize = len(xvals)
            # histsize = len(history_buf)
            # allvals =  list(histfill) + history_buf
            # totalsize = len(allvals)
            # print(f"{histsize} -- {xvalsize}  {totalsize}")
            
            allvals = list(histfill) + dewhistory_buf  
            
            #print(allvals)
            #print(xvals)
            ##allvals.reverse()
            bx.plot(xvals,allvals,'-', color="red")
            bx.invert_xaxis()
            bx.set_xlabel('Time "Current time is 0" (s)')
            bx.set_ylabel('Temperature (C)')
            bx.set_title(" Dew Point Sensor (3)")
            bx.grid()
            # Save it to a temporary buffer.
            buf = BytesIO()
            fig.savefig(buf,dpi=100, format="jpg")
            # Embed the result in the html output.
            data = base64.b64encode(buf.getbuffer()).decode("ascii")
            img_ascii = f"<img src='data:image/png;base64,{data}'/>"
            self.sockio.emit('hmd_send',img_ascii )

            try:
                time.sleep(self.updateinterval)
            except KeyboardInterrupt:
                print("Background monitor terminated...")
                self.db_con.close()
                sys.exit()
                # to be handled Snap7Exception:
            else:
                continue
            
            """
            Finally, any singleton should define some business logic, which can be
            executed on its instance.
            """

def init_db():
    db_con = sqlite3.connect("static/combinedHistory.db ")
    cursor = db_con.cursor()
    cursor.execute("CREATE TABLE temperature_records \
     (pos INTEGER, date REAL, \
      c1 REAL, c2 REAL, c3 REAL, c4 REAL , c5 REAL, c6 REAL, c7 REAL, c8 REAL , \
      c9 REAL, c10 REAL, c11 REAL, c12 REAL , c13 REAL, c14 REAL, c15 REAL, c16 REAL ,  \
      c17 REAL, c18 REAL, c19 REAL, c20 REAL , c21 REAL, c22 REAL, c23 REAL, c24 REAL)")
    cursor.execute("CREATE TABLE humidity_records \
     ( date REAL, \
      t1 REAL, t2 REAL, t3 REAL, t4 REAL , t5 REAL, t6 REAL, t7 REAL, t8 REAL , \
      rh1 REAL, rh2 REAL, rh3 REAL, rh4 REAL , rh5 REAL, rh6 REAL, rh7 REAL, rh8 REAL ,  \
      dew1 REAL, dew2 REAL, dew3 REAL, dew4 REAL , dew5 REAL, dew6 REAL, dew7 REAL, dew8 REAL )")
   
    db_con.commit()
    db_con.close()
