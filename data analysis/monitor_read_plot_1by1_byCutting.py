# -*- coding: utf-8 -*-
"""
Created on Tue Sep  7 15:22:19 2021

@author: oadiyama
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

df=pd.read_csv("monitor_data_20211013_350W_20deg.txt")

s_1=df.iloc[1::2,2] # temp values for the 1st sensor of the card 1
s_1=s_1[s_1>18]
s_2=df.iloc[1::2,3] # temp values for the 2st sensor of the card 1
s_2=s_2[s_2>18]
s_3=df.iloc[1::2,4] # temp values for the 3rd sensor of the card 1
s_3=s_3[s_3>18]
s_4=df.iloc[1::2,5]
s_4=s_4[s_4>18] 
s_5=df.iloc[1::2,6]
s_5=s_5[s_5>18] 
s_6=df.iloc[1::2,7]
s_6=s_6[s_6>18] 
s_7=df.iloc[1::2,8]
s_7=s_7[s_7>18] 
s_8=df.iloc[1::2,9]
s_8=s_8[s_8>18] 
s_9=df.iloc[1::2,10]
s_9=s_9[s_9>18] 
s_10=df.iloc[1::2,11]
s_10=s_10[s_10>18]
s_11=df.iloc[1::2,12]
s_11=s_11[s_11>18]
s_12=df.iloc[1::2,13]
s_12=s_12[s_12>18]
s_13=df.iloc[1::2,14]
s_13=s_13[s_13>18]
s_14=df.iloc[1::2,15]
s_14=s_14[s_14>18]
s_15=df.iloc[1::2,16]
s_15=s_15[s_15>18]
s_16=df.iloc[1::2,17]
s_16=s_16[s_16>18]
s_17=df.iloc[1::2,18]
s_17=s_17[s_17>18]
s_18=df.iloc[1::2,19]
s_18=s_18[s_18>18]
s_19=df.iloc[1::2,20]
s_19=s_19[s_19>18]
s_20=df.iloc[1::2,21]
s_20=s_20[s_20>18]
s_21=df.iloc[1::2,22]
s_21=s_21[s_21>18]
s_22=df.iloc[1::2,23]
s_22=s_22[s_22>18]
s_23=df.iloc[1::2,24]
s_23=s_23[s_23>18]
s_24=df.iloc[1::2,25] # temp values for the 24th sensor of the card 1
s_24=s_24[s_24>18]


plt.figure(1)
sns.distplot(s_1).set(title='20')
plt.figure(2)
sns.distplot(s_2).set(title='20')
plt.figure(3)
sns.distplot(s_3).set(title='20')
plt.figure(4)
sns.distplot(s_4).set(title='20')
plt.figure(5)
sns.distplot(s_5).set(title='20')
plt.figure(6)
sns.distplot(s_6).set(title='20')
plt.figure(7)
sns.distplot(s_7).set(title='20')
plt.figure(8)
sns.distplot(s_8).set(title='20')
plt.figure(9)
sns.distplot(s_9).set(title='20')
plt.figure(10)
sns.distplot(s_10).set(title='20')
plt.figure(11)
sns.distplot(s_11).set(title='20')
plt.figure(12)
sns.distplot(s_12).set(title='20')
plt.figure(13)
sns.distplot(s_13).set(title='20')
plt.figure(14)
sns.distplot(s_14).set(title='20')
plt.figure(15)
sns.distplot(s_15).set(title='20')
plt.figure(16)
sns.distplot(s_16).set(title='20')
plt.figure(17)
sns.distplot(s_17).set(title='20')
plt.figure(18)
sns.distplot(s_18).set(title='20')
plt.figure(19)
sns.distplot(s_19).set(title='20')
plt.figure(20)
sns.distplot(s_20).set(title='20')
plt.figure(21)
sns.distplot(s_21).set(title='20')
plt.figure(22)
sns.distplot(s_22).set(title='20')
plt.figure(23)
sns.distplot(s_23).set(title='20')
plt.figure(24)
sns.distplot(s_24).set(title='20')


print("s1:",np.mean(s_1),np.std(s_1))
print("s2:",np.mean(s_2),np.std(s_2))
print("s3:",np.mean(s_3),np.std(s_3))
print("s4:",np.mean(s_4),np.std(s_4))
print("s5:",np.mean(s_5),np.std(s_5))
print("s6:",np.mean(s_6),np.std(s_6))
print("s7:",np.mean(s_7),np.std(s_7))
print("s8:",np.mean(s_8),np.std(s_8))
print("s9:",np.mean(s_9),np.std(s_9))
print("s10:",np.mean(s_10),np.std(s_10))
print("s11:",np.mean(s_11),np.std(s_11))
print("s12:",np.mean(s_12),np.std(s_12))
print("s13:",np.mean(s_13),np.std(s_13))
print("s14:",np.mean(s_14),np.std(s_14))
print("s15:",np.mean(s_15),np.std(s_15))
print("s16:",np.mean(s_16),np.std(s_16))
print("s17:",np.mean(s_17),np.std(s_17))
print("s18:",np.mean(s_18),np.std(s_18))
print("s19:",np.mean(s_19),np.std(s_19))
print("s20:",np.mean(s_20),np.std(s_20))
print("s21:",np.mean(s_21),np.std(s_21))
print("s22:",np.mean(s_22),np.std(s_22))
print("s23:",np.mean(s_23),np.std(s_23))
print("s24:",np.mean(s_24),np.std(s_24))