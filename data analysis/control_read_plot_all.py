# -*- coding: utf-8 -*-
"""
Created on Tue Sep  7 17:01:03 2021

@author: oadiyama
"""

import pandas as pd

df=pd.read_csv("control_data minus 40.txt")

s_1=df.iloc[:,1] # temp values for the 1st sensor
s_2=df.iloc[:,2] # temp values for the 2st sensor
s_3=df.iloc[:,3] # temp values for the 3rd sensor
s_4=df.iloc[:,4] 
s_5=df.iloc[:,5] 
s_6=df.iloc[:,6] 
s_7=df.iloc[:,7] 
s_8=df.iloc[:,8] # temp values for the 8th sensor

import matplotlib.pyplot as plt
import seaborn as sns
sns.distplot(s_1).set(title='minus 40')
sns.distplot(s_2)
sns.distplot(s_3)
sns.distplot(s_4)
sns.distplot(s_5)
sns.distplot(s_6)
sns.distplot(s_7)
sns.distplot(s_8)
plt.xlabel("temperature")