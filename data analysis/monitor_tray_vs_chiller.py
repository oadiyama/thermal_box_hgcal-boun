# -*- coding: utf-8 -*-
"""
Created on Wed Sep 22 13:09:51 2021

@author: oadiyama
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from numpy import arange
from scipy.optimize import curve_fit
df=pd.read_csv("trays mean.txt")
print(df)

x=np.array([-40,-30,-20,-10,0])

i=0
while i<8:
    plt.figure(i+1)
    y=df.iloc[i,1:6]
    def objective(x, a, b):
        return a*x + b
    popt, _ = curve_fit(objective, x, y)
    a, b = popt
    print('y = %.5f * x + %.5f' % (a, b))
    plt.scatter(x,y,color='green')
    x_line = arange(min(x), max(x), 1)
    y_line = objective(x_line, a, b)
    plt.plot(x_line, y_line, '--', color='red')
    plt.ylabel('tray temperature')
    plt.xlabel('chiller temperature')
    plt.legend(['slope:%.5f'%a],handlelength=0)
    num=(i+1)
    plt.title('Tray%d'%num)
    i=i+1
