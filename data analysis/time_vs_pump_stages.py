# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 13:27:41 2021

@author: oadiyama
"""
import matplotlib.pyplot as plt
x=[1,2,3,4]
y1=[(4*3600+10*60+33)/3600,(3*3600+48*60+34)/3600,(3*3600+31*60+35)/3600,(3*3600+20*60+48)/3600]
y2=[(4*3600+25*60+53)/3600,(4*3600+4*60+58)/3600,(3*3600+42*60+44)/3600,(3*3600+42*60+31)/3600]
y3=[(2*3600+55*60+46)/3600,(2*3600+31*60+9)/3600,(2*3600+30*60+19)/3600,(1*3600+37*60+44)/3600]
y4=[(4*3600+13*60+13)/3600,(3*3600+38*60+12)/3600,(2*3600+53*60+49)/3600,(2*3600+54*60+42)/3600]
plt.figure(1)
plt.plot(x,y1,'*r')
plt.plot(x,y1,'g')
plt.title('Top Tray & from 20 to -40')
plt.xlabel('Pump Stages')
plt.ylabel('Convergence Time (hr)')
plt.figure(2)
plt.plot(x,y2,'*r')
plt.plot(x,y2,'g')
plt.title('Bottom Tray & from 20 to -40')
plt.xlabel('Pump Stages')
plt.ylabel('Convergence Time (hr)')
plt.figure(3)
plt.plot(x,y3,'*r')
plt.plot(x,y3,'g')
plt.title('Top Tray & from -40 to 20')
plt.xlabel('Pump Stages')
plt.ylabel('Convergence Time (hr)')
plt.figure(4)
plt.plot(x,y4,'*r')
plt.plot(x,y4,'g')
plt.title('Bottom Tray & from -40 to 20')
plt.xlabel('Pump Stages')
plt.ylabel('Convergence Time (hr)')