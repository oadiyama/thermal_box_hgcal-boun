# -*- coding: utf-8 -*-
"""
Created on Mon Sep 13 15:58:41 2021

@author: oadiyama
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

df=pd.read_csv("monitor_data_20211214_131948_stage4.txt")

a=0
b=0 
i=-1
for z in df.iloc[:,1]:  
    i=i+1
    if z=='2021-12-15 05:30:04': # starting time    
        a=i
        break

k=-1    
for w in df.iloc[:,1]:
    k=k+1
    if w=='2021-12-15 08:37:59': # finish time
        b=k
        break
b=b+1

plt.figure(1)
sns.displot(df.iloc[a:b:2,2])
plt.figure(2)
sns.displot(df.iloc[a:b:2,3])
plt.figure(3)
sns.displot(df.iloc[a:b:2,4])
plt.figure(4)
sns.displot(df.iloc[a:b:2,5])
plt.figure(5)
sns.displot(df.iloc[a:b:2,6])
plt.figure(6)
sns.displot(df.iloc[a:b:2,7])
plt.figure(7)
sns.displot(df.iloc[a:b:2,8])
plt.figure(8)
sns.displot(df.iloc[a:b:2,9])
plt.figure(9)
sns.displot(df.iloc[a:b:2,10])
plt.figure(10)
sns.displot(df.iloc[a:b:2,11])
plt.figure(11)
sns.displot(df.iloc[a:b:2,12])
plt.figure(12)
sns.displot(df.iloc[a:b:2,13])
plt.figure(13)
sns.displot(df.iloc[a:b:2,14])
plt.figure(14)
sns.displot(df.iloc[a:b:2,15])
plt.figure(15)
sns.displot(df.iloc[a:b:2,16])
plt.figure(16)
sns.displot(df.iloc[a:b:2,17])
plt.figure(17)
sns.displot(df.iloc[a:b:2,18])
plt.figure(18)
sns.displot(df.iloc[a:b:2,19])
plt.figure(19)
sns.displot(df.iloc[a:b:2,20])
plt.figure(20)
sns.displot(df.iloc[a:b:2,21])
plt.figure(21)
sns.displot(df.iloc[a:b:2,22])
plt.figure(22)
sns.displot(df.iloc[a:b:2,23])
plt.figure(23)
sns.displot(df.iloc[a:b:2,24])
plt.figure(24)
sns.displot(df.iloc[a:b:2,25])


print("s1:",np.mean(df.iloc[a:b:2,2]),np.std(df.iloc[a:b:2,2]))
print("s2:",np.mean(df.iloc[a:b:2,3]),np.std(df.iloc[a:b:2,3]))
print("s3:",np.mean(df.iloc[a:b:2,4]),np.std(df.iloc[a:b:2,4]))
print("s4:",np.mean(df.iloc[a:b:2,5]),np.std(df.iloc[a:b:2,5]))
print("s5:",np.mean(df.iloc[a:b:2,6]),np.std(df.iloc[a:b:2,6]))
print("s6:",np.mean(df.iloc[a:b:2,7]),np.std(df.iloc[a:b:2,7]))
print("s7:",np.mean(df.iloc[a:b:2,8]),np.std(df.iloc[a:b:2,8]))
print("s8:",np.mean(df.iloc[a:b:2,9]),np.std(df.iloc[a:b:2,9]))
print("s9:",np.mean(df.iloc[a:b:2,10]),np.std(df.iloc[a:b:2,10]))
print("s10:",np.mean(df.iloc[a:b:2,11]),np.std(df.iloc[a:b:2,11]))
print("s11:",np.mean(df.iloc[a:b:2,12]),np.std(df.iloc[a:b:2,12]))
print("s12:",np.mean(df.iloc[a:b:2,13]),np.std(df.iloc[a:b:2,13]))
print("s13:",np.mean(df.iloc[a:b:2,14]),np.std(df.iloc[a:b:2,14]))
print("s14:",np.mean(df.iloc[a:b:2,15]),np.std(df.iloc[a:b:2,15]))
print("s15:",np.mean(df.iloc[a:b:2,16]),np.std(df.iloc[a:b:2,16]))
print("s16:",np.mean(df.iloc[a:b:2,17]),np.std(df.iloc[a:b:2,17]))
print("s17:",np.mean(df.iloc[a:b:2,18]),np.std(df.iloc[a:b:2,18]))
print("s18:",np.mean(df.iloc[a:b:2,19]),np.std(df.iloc[a:b:2,19]))
print("s19:",np.mean(df.iloc[a:b:2,20]),np.std(df.iloc[a:b:2,20]))
print("s20:",np.mean(df.iloc[a:b:2,21]),np.std(df.iloc[a:b:2,21]))
print("s21:",np.mean(df.iloc[a:b:2,22]),np.std(df.iloc[a:b:2,22]))
print("s22:",np.mean(df.iloc[a:b:2,23]),np.std(df.iloc[a:b:2,23]))
print("s23:",np.mean(df.iloc[a:b:2,24]),np.std(df.iloc[a:b:2,24]))
print("s24:",np.mean(df.iloc[a:b:2,25]),np.std(df.iloc[a:b:2,25]))

