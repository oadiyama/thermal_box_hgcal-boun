# -*- coding: utf-8 -*-
"""
Created on Tue Sep  7 15:22:19 2021

@author: oadiyama
"""

import pandas as pd

df=pd.read_csv("monitor_data minus 40.txt")

s_1=df.iloc[::2,2] # temp values for the 1st sensor of the card 1
s_2=df.iloc[::2,3] # temp values for the 2st sensor of the card 1
s_3=df.iloc[::2,4] # temp values for the 3rd sensor of the card 1
s_4=df.iloc[::2,5] 
s_5=df.iloc[::2,6] 
s_6=df.iloc[::2,7] 
s_7=df.iloc[::2,8] 
s_8=df.iloc[::2,9] 
s_9=df.iloc[::2,10] 
s_10=df.iloc[::2,11]
s_11=df.iloc[::2,12]
s_12=df.iloc[::2,13]
s_13=df.iloc[::2,14]
s_14=df.iloc[::2,15]
s_15=df.iloc[::2,16]
s_16=df.iloc[::2,17]
s_17=df.iloc[::2,18]
s_18=df.iloc[::2,19]
s_19=df.iloc[::2,20]
s_20=df.iloc[::2,21]
s_21=df.iloc[::2,22]
s_22=df.iloc[::2,23]
s_23=df.iloc[::2,24]
s_24=df.iloc[::2,25] # temp values for the 24th sensor of the card 1

import matplotlib.pyplot as plt
import seaborn as sns
sns.distplot(s_1).set(title='minus 40')
sns.distplot(s_2)
sns.distplot(s_3)
sns.distplot(s_4)
sns.distplot(s_5)
sns.distplot(s_6)
sns.distplot(s_7)
sns.distplot(s_8)
sns.distplot(s_9)
sns.distplot(s_10)
sns.distplot(s_11)
sns.distplot(s_12)
sns.distplot(s_13)
sns.distplot(s_14)
sns.distplot(s_15)
sns.distplot(s_16)
sns.distplot(s_17)
sns.distplot(s_18)
sns.distplot(s_19)
sns.distplot(s_20)
sns.distplot(s_21)
sns.distplot(s_22)
sns.distplot(s_23)
sns.distplot(s_24)
plt.xlabel("temperature")