# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 12:42:48 2021

@author: oadiyama
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

df=pd.read_csv("monitor_data_no_powerminus35toplus20.txt")

a=0
b=0
i=-1
for z in df.iloc[:,1]:
    i=i+1
    if z=='2021-10-12 20:19:32': # starting time    
        a=i
        break

k=-1
for w in df.iloc[:,1]:
    k=k+1
    if w=='2021-10-12 21:20:02': # finish time
        b=k
        break
b=b+1

# plt.figure(1)
# sns.displot(df.iloc[a:b:2,2:25]).set(title='zero')

sensors=np.array([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24])

mean=np.array([np.mean(df.iloc[a:b:2,2]),
               np.mean(df.iloc[a:b:2,3]),
               np.mean(df.iloc[a:b:2,4]),
               np.mean(df.iloc[a:b:2,5]),
               np.mean(df.iloc[a:b:2,6]),
               np.mean(df.iloc[a:b:2,7]),
               np.mean(df.iloc[a:b:2,8]),
               np.mean(df.iloc[a:b:2,9]),
               np.mean(df.iloc[a:b:2,10]),
               np.mean(df.iloc[a:b:2,11]),
               np.mean(df.iloc[a:b:2,12]),
               np.mean(df.iloc[a:b:2,13]),
               np.mean(df.iloc[a:b:2,14]),
               np.mean(df.iloc[a:b:2,15]),
               np.mean(df.iloc[a:b:2,16]),
               np.mean(df.iloc[a:b:2,17]),
               np.mean(df.iloc[a:b:2,18]),
               np.mean(df.iloc[a:b:2,19]),
               np.mean(df.iloc[a:b:2,20]),
               np.mean(df.iloc[a:b:2,21]),
               np.mean(df.iloc[a:b:2,22]),
               np.mean(df.iloc[a:b:2,23]),
               np.mean(df.iloc[a:b:2,24]),
               np.mean(df.iloc[a:b:2,25]),])

error=np.array([np.std(df.iloc[a:b:2,2]),
               np.std(df.iloc[a:b:2,3]),
               np.std(df.iloc[a:b:2,4]),
               np.std(df.iloc[a:b:2,5]),
               np.std(df.iloc[a:b:2,6]),
               np.std(df.iloc[a:b:2,7]),
               np.std(df.iloc[a:b:2,8]),
               np.std(df.iloc[a:b:2,9]),
               np.std(df.iloc[a:b:2,10]),
               np.std(df.iloc[a:b:2,11]),
               np.std(df.iloc[a:b:2,12]),
               np.std(df.iloc[a:b:2,13]),
               np.std(df.iloc[a:b:2,14]),
               np.std(df.iloc[a:b:2,15]),
               np.std(df.iloc[a:b:2,16]),
               np.std(df.iloc[a:b:2,17]),
               np.std(df.iloc[a:b:2,18]),
               np.std(df.iloc[a:b:2,19]),
               np.std(df.iloc[a:b:2,20]),
               np.std(df.iloc[a:b:2,21]),
               np.std(df.iloc[a:b:2,22]),
               np.std(df.iloc[a:b:2,23]),
               np.std(df.iloc[a:b:2,24]),
               np.std(df.iloc[a:b:2,25]),])

plt.figure(2)
plt.errorbar(sensors, mean, yerr=error, fmt='.g', ecolor='r')
plt.legend('σ')
plt.plot(sensors,mean)
plt.title('20')
plt.xlabel('sensors')
plt.ylabel('mean values')
plt.show()
