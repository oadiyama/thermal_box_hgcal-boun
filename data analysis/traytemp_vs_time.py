# -*- coding: utf-8 -*-
"""
Created on Wed Sep 22 09:37:00 2021

@author: oadiyama
"""
    
import pandas as pd
import numpy as np
from datetime import timedelta
import matplotlib.pyplot as plt
import math
df=pd.read_csv("monitor_data_20220121_120030 - Copy.txt")
print(df)
time1=df['time']
time=df['time'].to_numpy()

i=0
j=0
traytop=np.zeros(int(len(df.index)/2))
traybottom=np.zeros(int(len(df.index)/2))
while i<len(df.index):
    traytop[j]=np.mean(df.iloc[i,3:8])
    traybottom[j]=np.mean(df.iloc[i+1,21:])
    i=i+2
    j=j+1
x2=pd.to_datetime(df.iloc[-1,1])-pd.to_datetime(df.iloc[1,1])
x2=int((x2.total_seconds()))/3600
x_ticks1=[]
x_ticks2=[]
x_labels=[]
k=0
while k/2<x2:
    x_ticks1.append(time[0+(360*k)])
    x_ticks2.append(time[1+(360*k)])
    x_labels.append(k/2)
    k=k+1
plt.figure(1)
plt.plot(time[0::2],traytop)
plt.xticks(ticks=x_ticks1, labels=x_labels)
plt.title('Top Tray')
plt.xlabel('Time (hours)')
plt.ylabel('Temperature (°C)')
plt.figure(2)
plt.plot(time[1::2],traybottom)
plt.xticks(ticks=x_ticks2, labels=x_labels)
plt.title('Bottom Tray')
plt.xlabel('Time (hours)')
plt.ylabel('Temperature (°C)')

# TOP Side
a=0
i=0
k=0
delta1= timedelta(days=0,seconds=0,minutes=0,hours=1)
delta2= timedelta(days=0,seconds=0,minutes=55,hours=0)
delta3= timedelta(days=0,seconds=45,minutes=59,hours=0)
for z in df.iloc[2::2,1]:
    i=i+2
    z=pd.to_datetime(z)
    x=df.iloc[k,1]
    x=pd.to_datetime(x)
    if z-x<delta1 and z-x>delta2 and abs(traytop[int(i/2)]-traytop[int(k/2)])<0.5:     
        a=i
        break
    if z-x>=delta3:
        k=k+2
print("Stability time for the top:",z)
y=df.iloc[0,1]
y=pd.to_datetime(y)
print("Top Tray Time Interval:",z-y)
plt.figure(3)
plt.plot(time[0:a:2],traytop[0:int(a/2)])
plt.xticks(ticks=x_ticks1, labels=x_labels)
plt.title('Top Tray Before the Goal')
plt.xlabel('Time (hours)')
plt.ylabel('Temperature (°C)')
plt.figure(4)
plt.plot(time[a::2],traytop[int(a/2):])
plt.xticks(ticks=x_ticks1, labels=x_labels)
plt.title('Top Tray After the Goal')
plt.xlabel('Time (hours)')
plt.ylabel('Temperature (°C)')

# BOTTOM Side
a=0
i=1
k=1
delta1= timedelta(days=0,seconds=0,minutes=0,hours=1)
delta2= timedelta(days=0,seconds=0,minutes=55,hours=0)
delta3= timedelta(days=0,seconds=45,minutes=59,hours=0)
for z in df.iloc[3::2,1]:
    i=i+2
    z=pd.to_datetime(z)
    x=df.iloc[k,1]
    x=pd.to_datetime(x)
    if z-x<delta1 and z-x>delta2 and abs(traybottom[int((i-1)/2)]-traybottom[int((k-1)/2)])<0.5:     
        a=i
        break
    if z-x>=delta3:
        k=k+2
print("Stability time for the bottom:",z)
y=df.iloc[1,1]
y=pd.to_datetime(y)
print("Bottom Tray Time Interval:",z-y)
plt.figure(5)
plt.plot(time[1:a:2],traybottom[0:int((a-1)/2)])
plt.xticks(ticks=x_ticks2, labels=x_labels)
plt.title('Bottom Tray Before the Goal')
plt.xlabel('Time (hours)')
plt.ylabel('Temperature (°C)')
plt.figure(6)
plt.plot(time[a::2],traybottom[int((a-1)/2):])
plt.xticks(ticks=x_ticks2, labels=x_labels)
plt.title('Bottom Tray After the Goal')
plt.xlabel('Time (hours)')
plt.ylabel('Temperature (°C)')

